<?php

/**
 * ether_wake device delete view.
 *
 * @category   apps
 * @package    app-ether_wake
 * @subpackage views
 * @author     AvanTech <fabio.montefuscolo@gmail.com>
 * @copyright  2018 AvanTech
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://gitlab.com/fabiomontefuscolo/app-ether-wake/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\ether_wake;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

use \clearos\apps\base\Engine_Exception as Engine_Exception;
clearos_load_library('base/Engine_Exception');


///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('ether_wake');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * ether_wake main class.
 *
 * @category   apps
 * @package    app-ether_wake
 * @subpackage libraries
 * @author     AvanTech <fabio.montefuscolo@gmail.com>
 * @copyright  2018 AvanTech
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://gitlab.com/fabiomontefuscolo/app-ether-wake/
 */

class Schedule
{
    const DATABASE_PATH = '/var/clearos/ether_wake/ether_wake.db';
    const SCHEDULE_PATH = '/etc/cron.d/app-ether_wake';
    private $pdo;

    public function __construct()
    {
        $this->pdo = new \PDO('sqlite:' . self::DATABASE_PATH);
    }

    public function delete($schedule_id)
    {
        if(!is_numeric($schedule_id)) {
            return false;
        }

        $stmt = $this->pdo->prepare("DELETE FROM schedule WHERE schedule_id = :schedule_id");
        $stmt->bindParam(':schedule_id', $schedule_id);

        return $stmt->execute();
    }

    public function save($data)
    {
        $stmt = $this->pdo->prepare("
            REPLACE INTO schedule(schedule_id, device_id, active, minute, hour, dom, month, dow)
                VALUES(:schedule_id, :device_id, :active, :minute, :hour, :dom, :month, :dow);
        ");

        // it means update
        $id = null;
        if(isset($data['schedule_id']) && is_numeric($data['schedule_id'])) {
            $id = intval($data['schedule_id'], 10);
            $stmt->bindParam(':schedule_id',      $data['schedule_id']);
        }
        $stmt->bindParam(':device_id', $data['device_id']);
        $stmt->bindParam(':active',    $data['active']);
        $stmt->bindParam(':minute',    $data['minute']);
        $stmt->bindParam(':hour',      $data['hour']);
        $stmt->bindParam(':dom',       $data['dom']);
        $stmt->bindParam(':month',     $data['month']);
        $stmt->bindParam(':dow',       $data['dow']);

        if($stmt->execute()) {
            $schedule_id = $schedule_id ?: $this->pdo->lastInsertId();
            return $this->get($schedule_id);
        }
    }

    public function get($schedule_id, $options=array())
    {
        $options = is_array($options) ? $options : array();
        $options = array_merge(array(
            'select_related' => false
        ), $options);

        $sql = "SELECT * FROM schedule s";
        if($options['select_related'] === true) {
            $sql .= ' INNER JOIN devices d ON d.device_id = s.device_id';
        }
        $sql .= '  WHERE s.schedule_id = :schedule_id;';

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':schedule_id', $schedule_id);

        if ($stmt->execute()) {
            return $stmt->fetch(\PDO::FETCH_ASSOC);
        }
    }

    public function list_entries($options=array())
    {
        $options = is_array($options) ? $options : array();
        $options = array_merge(array(
            'device_id' => null,
            'select_related' => false
        ), $options);

        $sql = "SELECT * FROM schedule s";

        if (is_numeric($options['device_id'])) {
            $sql .= sprintf(' WHERE s.device_id = %d', $options['device_id']);
        }

        if ($options['select_related'] === true) {
            $sql .= ' INNER JOIN devices d ON s.device_id = d.device_id';
        }

        $sql .= ' ORDER BY s.schedule_id';
        $stmt = $this->pdo->prepare($sql);

        if ($stmt->execute()) {
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } else {
            var_dump($stmt->errorInfo());
        }
    }

    public function install_schedule($commit=true)
    {
        $croncontent = '';

        $entries = $this->list_entries(array('select_related' => true));
        $cl = array_reduce($entries, function($length, $entry){
            return max(
                $length,
                strlen($entry['minute']),
                strlen($entry['hour']),
                strlen($entry['dom']),
                strlen($entry['month']),
                strlen($entry['dow'])
            );
        }, 1);

        foreach ($entries as $entry) {
            $croncontent = $croncontent . sprintf(
                "%{$cl}s %{$cl}s %{$cl}s %{$cl}s %{$cl}s %s\n",
                $entry['minute'],
                $entry['hour'],
                $entry['dom'],
                $entry['month'],
                $entry['dow'],
                "/usr/sbin/ether-wake {$entry['address']}"
            );
        }

        $pipes = array();
        $descriptorspec = array(
            0 => array("pipe", "r"),
            1 => array("pipe", "w"),
            2 => array("pipe", "w"),
            3 => array("pipe", "w")
        );

        // Run this command first: /usr/sbin/addsudo /usr/bin/tee app-ether_wake
        $commandLine = 'sudo /usr/bin/tee ' . self::SCHEDULE_PATH . '; echo $? >&3';
        $process = proc_open($commandLine, $descriptorspec, $pipes);

        // stdin
        fwrite($pipes[0], $croncontent);
        fclose($pipes[0]);

        // stdout
        $out = stream_get_contents($pipes[1]);
        fclose($pipes[1]);

        // stderr
        $err = stream_get_contents($pipes[2]);
        fclose($pipes[2]);

        // ret
        $ret = intval(stream_get_contents($pipes[3]), 10);
        fclose($pipes[3]);

        if($ret !== 0) {
            throw new Engine_Exception($err, CLEAROS_ERROR);
        }

        return $out;
    }

    public function get_schedule_content()
    {
        if(is_readable(self::SCHEDULE_PATH)) {
            return file_get_contents(self::SCHEDULE_PATH);
        }
        return '';
    }
}
