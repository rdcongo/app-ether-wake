<?php

/**
 * ether_wake device delete view.
 *
 * @category   apps
 * @package    app-ether_wake
 * @subpackage views
 * @author     AvanTech <fabio.montefuscolo@gmail.com>
 * @copyright  2018 AvanTech
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://gitlab.com/fabiomontefuscolo/app-ether-wake/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\ether_wake;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('ether_wake');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * ether_wake main class.
 *
 * @category   apps
 * @package    app-ether_wake
 * @subpackage libraries
 * @author     AvanTech <fabio.montefuscolo@gmail.com>
 * @copyright  2018 AvanTech
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://gitlab.com/fabiomontefuscolo/app-ether-wake/
 */

class Ether_Wake
{
    const DATABASE_PATH = '/var/clearos/ether_wake/ether_wake.db';
    private $pdo;

    public function __construct()
    {
        $this->pdo = new \PDO('sqlite:' . self::DATABASE_PATH);
    }

    public function delete_device($device_id)
    {
        if(!is_numeric($device_id)) {
            return false;
        }

        $stmt = $this->pdo->prepare("DELETE FROM devices WHERE device_id = :device_id");
        $stmt->bindParam(':device_id', $device_id);

        return $stmt->execute();
    }

    public function save_device($data)
    {
        $stmt = $this->pdo->prepare("
            REPLACE INTO devices(device_id, name, address)
                VALUES(:id, :name, :address);
        ");

        // it means update
        $device_id = null;
        if(isset($data['device_id']) && is_numeric($data['device_id'])) {
            $device_id = intval($data['device_id'], 10);
            $stmt->bindParam(':device_id', $data['device_id']);
        }

        $stmt->bindParam(':name',        $data['name']);
        $stmt->bindParam(':address',     $data['address']);

        if($stmt->execute()) {
            $device_id = $device_id ?: $this->pdo->lastInsertId();
            return $this->get_device($device_id);
        }
    }

    public function get_device($device_id)
    {
        $stmt = $this->pdo->prepare("SELECT * FROM devices WHERE device_id = :device_id");
        $stmt->bindParam(':device_id', $device_id);

        if ($stmt->execute()) {
            return $stmt->fetch(\PDO::FETCH_ASSOC);
        }
    }

    public function list_devices()
    {
        $stmt = $this->pdo->prepare("SELECT * FROM devices ORDER BY name");

        if ($stmt->execute()) {
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
    }

    public function wake_device($device_id)
    {
        $device = $this->get_device($device_id);

        if(empty($device) || empty($device['address'])) {
            return;
        }

        $command = "sudo /usr/sbin/ether-wake {$device['address']}";

        $status = -1;
        $result = system($command, $status);
        return $status === 0;
    }


}
