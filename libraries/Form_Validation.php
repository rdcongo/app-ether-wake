<?php

/**
 * ether_wake form validation class
 *
 * @category   apps
 * @package    app-ether_wake
 * @subpackage libraries
 * @author     AvanTech <fabio.montefuscolo@gmail.com>
 * @copyright  2018 AvanTech
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://gitlab.com/fabiomontefuscolo/app-ether-wake/
 */


///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\ether_wake;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('ether_wake');


///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * ether_wake form validation class
 *
 * @category   apps
 * @package    app-ether_wake
 * @subpackage libraries
 * @author     AvanTech <fabio.montefuscolo@gmail.com>
 * @copyright  2018 AvanTech
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://gitlab.com/fabiomontefuscolo/app-ether-wake/
 */

class Form_Validation {

    public function validate_device_id($arg)
    {
        $ok = is_numeric($arg)
            && preg_match('/^\d+$/', $arg);
        
        if(!$ok) return "Invalid value";
    }

    public function validate_device_name($arg)
    {
        $ok = is_string($arg)
            && preg_match('/^[\w. -]{3,}$/', $arg);
        
        if(!$ok) return "Invalid value";
    }

    public function validate_device_address($arg)
    {
        $ok = is_string($arg)
            && preg_match('/^[a-fA-F0-9][a-fA-F0-9](:[a-fA-F0-9][a-fA-F0-9]){5}$/', $arg);
        
        if(!$ok) return "Invalid value";
    }

    //
    // Schedule validation
    //
    public function validate_schedule_id($arg)
    {
        $ok = is_numeric($arg)
            && preg_match('/^\d+$/', $arg);
        
        if(!$ok) return "Invalid value";
    }

    public function validate_schedule_minute($arg)
    {
        if (trim($arg) === '*') {
            return;
        }

        if (is_numeric($arg)) {
            $arg = intval($arg, 10);
            if($arg >= 0 && $arg < 60 && $arg % 1 === 0) {
                return;
            }
            return 'Invalid value';
        }

        if (preg_match('/^[0-6]?\d(,[0-6]?\d)+$/', $arg)) {
            $arg = explode(',', $arg);
            $arg = array_map('intval', $arg);
            
            if(min($arg) >= 0 && max($arg) < 60) {
                return;
            }
            return 'Invalid value';
        }

        if (preg_match('/^\*\/([0-6]?\d)$/', $arg, $match)) {
            $arg = intval($match[1]);
            if ($arg > 0 && $arg < 60) {
                return;
            }
            return 'Invalid value';
        }

        return 'Invalid value';
    }

    public function validate_schedule_hour($arg)
    {
        if (trim($arg) === '*') {
            return;
        }

        if (is_numeric($arg)) {
            $arg = intval($arg, 10);
            if($arg >= 0 && $arg < 24 && $arg % 1 === 0) {
                return;
            }
            return 'Invalid value';
        }

        if (preg_match('/^[0-2]?\d(,[0-2]?\d)+$/', $arg)) {
            $arg = explode(',', $arg);
            $arg = array_map('intval', $arg);
            
            if(min($arg) >= 0 && max($arg) < 24) {
                return;
            }
            return 'Invalid value';
        }

        if (preg_match('/^\*\/([0-2]?\d)$/', $arg, $match)) {
            $arg = intval($match[1]);
            if ($arg > 0 && $arg < 24) {
                return;
            }
            return 'Invalid value';
        }

        return 'Invalid value';
    }

    public function validate_schedule_dom($arg)
    {
        if (trim($arg) === '*') {
            return;
        }

        if (is_numeric($arg)) {
            $arg = intval($arg, 10);
            if($arg > 0 && $arg <= 31 && $arg % 1 === 0) {
                return;
            }
            return 'Invalid value';
        }

        if (preg_match('/^[1-3]?\d(,[1-3]?\d)+$/', $arg)) {
            $arg = explode(',', $arg);
            $arg = array_map('intval', $arg);
            
            if(min($arg) > 0 && max($arg) <= 31) {
                return;
            }
            return 'Invalid value';
        }

        if (preg_match('/^\*\/([1-3]?\d)$/', $arg, $match)) {
            $arg = intval($match[1]);
            if ($arg > 0 && $arg < 31) {
                return;
            }
            return 'Invalid value';
        }

        return 'Invalid value';
    }

    public function validate_schedule_month($arg)
    {
        if (trim($arg) === '*') {
            return;
        }

        if (is_numeric($arg)) {
            $arg = intval($arg, 10);
            if($arg > 0 && $arg <= 12 && $arg % 1 === 0) {
                return;
            }
            return 'Invalid value';
        }

        if (preg_match('/^[01]?\d(,[01]?\d)+$/', $arg)) {
            $arg = explode(',', $arg);
            $arg = array_map('intval', $arg);
            
            if(min($arg) > 0 && max($arg) <= 12) {
                return;
            }
            return 'Invalid value';
        }

        if (preg_match('/^\*\/([01]?\d)$/', $arg, $match)) {
            $arg = intval($match[1]);
            if ($arg > 0 && $arg < 12) {
                return;
            }
            return 'Invalid value';
        }

        return 'Invalid value';
    }

    public function validate_schedule_dow($arg)
    {
        if (trim($arg) === '*') {
            return;
        }

        if (is_numeric($arg)) {
            $arg = intval($arg, 10);
            if($arg >= 0 && $arg <= 6 && $arg % 1 === 0) {
                return;
            }
            return 'Invalid value';
        }

        if (preg_match('/^[0-6](,[0-6])+$/', $arg)) {
            $arg = explode(',', $arg);
            $arg = array_map('intval', $arg);
            
            if(min($arg) >= 0 && max($arg) <= 6) {
                return;
            }
            return 'Invalid value';
        }

        if (preg_match('/^\*\/([0-6])$/', $arg, $match)) {
            $arg = intval($match[1]);
            if ($arg > 0 && $arg <= 6) {
                return;
            }
            return 'Invalid value';
        }

        return 'Invalid value';
    }
}
