
Name: app-ether-wake
Epoch: 1
Version: 1.0.0
Release: 1%{dist}
Summary: Ethernet WakeUp
License: MIT
Group: Applications/Apps
Packager: Fabio Montefuscolo
Vendor: Vendor
Source: %{name}-%{version}.tar.gz
Buildarch: noarch
Requires: %{name}-core = 1:%{version}-%{release}
Requires: app-base
Requires: app-network

%description
Wake computer and other device on local network

%package core
Summary: Ethernet WakeUp - API
License: MIT
Group: Applications/API
Requires: app-base-core
Requires: app-network-core >= 1:2.4.2
Requires: net-tools >= 2.0

%description core
Wake computer and other device on local network

This package provides the core API and libraries.

%prep
%setup -q
%build

%install
mkdir -p -m 755 %{buildroot}/usr/clearos/apps/ether_wake
cp -r * %{buildroot}/usr/clearos/apps/ether_wake/
rm -f %{buildroot}/usr/clearos/apps/ether_wake/README.md
install -d -m 0755 %{buildroot}/var/clearos/ether_wake
install -d -m 0755 %{buildroot}/var/clearos/ether_wake/backup

%post
logger -p local6.notice -t installer 'app-ether-wake - installing'

%post core
logger -p local6.notice -t installer 'app-ether-wake-core - installing'

if [ $1 -eq 1 ]; then
    [ -x /usr/clearos/apps/ether_wake/deploy/install ] && /usr/clearos/apps/ether_wake/deploy/install
fi

[ -x /usr/clearos/apps/ether_wake/deploy/upgrade ] && /usr/clearos/apps/ether_wake/deploy/upgrade

exit 0

%preun
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-ether-wake - uninstalling'
fi

%preun core
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-ether-wake-core - uninstalling'
    [ -x /usr/clearos/apps/ether_wake/deploy/uninstall ] && /usr/clearos/apps/ether_wake/deploy/uninstall
fi

exit 0

%files
%defattr(-,root,root)
/usr/clearos/apps/ether_wake/controllers
/usr/clearos/apps/ether_wake/views

%files core
%defattr(-,root,root)
%doc README.md
%exclude /usr/clearos/apps/ether_wake/packaging
%doc README.md
%exclude /usr/clearos/apps/ether_wake/unify.json
%dir /usr/clearos/apps/ether_wake
%dir /var/clearos/ether_wake
%dir /var/clearos/ether_wake/backup
/usr/clearos/apps/ether_wake/deploy
/usr/clearos/apps/ether_wake/language
