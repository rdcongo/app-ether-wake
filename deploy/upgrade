#!/bin/bash

APP_DATA_DIR="/var/clearos/ether_wake"
APP_DB="${APP_DATA_DIR}/ether_wake.db"

# setup sudoers
/usr/sbin/addsudo /usr/sbin/ether-wake app-ether_wake
/usr/sbin/addsudo /usr/bin/tee         app-ether_wake

# create datadir
if [ ! -d "${APP_DATA_DIR}" ];
then
    mkdir -m 700 -p "${APP_DATA_DIR}";
fi

# create initial db
if [ ! -f "${APP_DB}" ];
then
    sqlite3 "${APP_DB}" <<'EOF'
        CREATE TABLE IF NOT EXISTS devices(
            device_id INTEGER PRIMARY KEY,
            created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            name VARCHAR(128) NOT NULL, 
            address VARCHAR(32) NOT NULL 
        );
        CREATE TABLE IF NOT EXISTS schedule(
            schedule_id INTEGER PRIMARY KEY,
            created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            device_id INTEGER NOT NULL REFERENCES devices(device_id) ON DELETE CASCADE,
            active TINYINT NOT NULL DEFAULT 1,
            minute VARCHAR(32) NOT NULL,
            hour VARCHAR(32) NOT NULL,
            dom VARCHAR(32) NOT NULL,
            month VARCHAR(32) NOT NULL,
            dow VARCHAR(32) NOT NULL
        );
        CREATE TRIGGER on_delete_device_cascade_to_schedule AFTER DELETE ON devices
        BEGIN
            DELETE FROM schedule WHERE device_id = old.device_id;
        END;
EOF
fi

chown -R webconfig: "${APP_DATA_DIR}"
