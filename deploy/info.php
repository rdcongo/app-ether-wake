<?php

/////////////////////////////////////////////////////////////////////////////
// General information
/////////////////////////////////////////////////////////////////////////////

$app['basename'] = 'ether_wake';
$app['version'] = '1.0.0';
$app['release'] = '1';
$app['vendor'] = 'Vendor';
$app['packager'] = 'Fabio Montefuscolo';
$app['license'] = 'GPLv3';
$app['license_core'] = 'LGPLv3';
$app['description'] = lang('ether_wake_app_description');
$app['tooltip'] = lang('ether_wake_app_description');

/////////////////////////////////////////////////////////////////////////////
// App name and categories
/////////////////////////////////////////////////////////////////////////////

$app['name'] = lang('ether_wake_app_name');
$app['category'] = lang('base_category_network');
$app['subcategory'] = lang('base_subcategory_infrastructure');


/////////////////////////////////////////////////////////////////////////////
// Controllers
/////////////////////////////////////////////////////////////////////////////

$app['controllers']['ether_wake']['title'] = lang('ether_wake_app_name');
$app['controllers']['settings']['title'] = lang('base_settings');

/////////////////////////////////////////////////////////////////////////////
// Packaging
/////////////////////////////////////////////////////////////////////////////

$app['requires'] = array(
    'app-network',
);

$app['core_requires'] = array(
    'app-network-core >= 1:2.4.2',
    'net-tools >= 2.0',
);

$app['core_directory_manifest'] = array(
    '/var/clearos/ether_wake' => array(),
    '/var/clearos/ether_wake/backup' => array(),
);
