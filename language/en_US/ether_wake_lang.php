<?php

$lang['ether_wake_app_description'] = 'Wake computers and other device on local network';
$lang['ether_wake_app_name'] = 'Ethernet WakeUp';
$lang['ether_wake_device_address'] = 'Address';
$lang['ether_wake_device_list'] = 'Devices list';
$lang['ether_wake_device_name'] = 'Name';
$lang['ether_wake_wol_packet_sent'] = 'WOL packet sent!';
$lang['ether_wake_schedule'] = 'Schedule';
$lang['ether_wake_schedule_add'] = 'Add schedule';
$lang['ether_wake_schedule_minute'] = 'Minute';
$lang['ether_wake_schedule_hour'] = 'Hour';
$lang['ether_wake_schedule_dom'] = 'Day of month';
$lang['ether_wake_schedule_month'] = 'Month';
$lang['ether_wake_schedule_dow'] = 'Day of week';
$lang['ether_wake_schedule_file_content'] = 'Schedule file content (CRON)';
$lang['ether_wake_delete_entry'] = 'Are you sure you want to delete this entry?';
