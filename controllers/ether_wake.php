<?php

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Hello World controller.
 *
 * @category   Apps
 * @package    ether-wake
 * @subpackage Controllers
 * @author     Fabio Montefuscolo <fabio.montefuscolo@gmail.com>
 * @copyright  2018 Fabio Montefuscolo / Avantech
 * @license    MIT
 */

class Ether_Wake extends ClearOS_Controller
{
    /**
     * Hello World default controller.
     *
     * @return view
     */

    function index()
    {
        // Load dependencies
        //------------------

        $this->lang->load('ether_wake');
        $this->lang->load('network');

        // Load views
        //-----------

        $views = array( 'ether_wake/device' );
        $this->page->view_forms($views, lang('ether_wake_app_name'));
    }
}
