<?php

/**
 * ether_wake schedule controller.
 *
 * @category   apps
 * @package    app-ether_wake
 * @subpackage views
 * @author     AvanTech <fabio.montefuscolo@gmail.com>
 * @copyright  2018 AvanTech
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://gitlab.com/fabiomontefuscolo/app-ether-wake/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

use \Exception as Exception;

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * OpenSSH server settings controller.
 *
 * @category   apps
 * @package    ether-wake
 * @subpackage controllers
 * @author     Fabio Montefuscolo <fabio.montefuscolo@gmail.com>
 * @copyright  2018 Avantech
 */

class Schedule extends ClearOS_Controller
{
    function index()
    {
        $this->load->library('ether_wake/Ether_Wake');

        $data = array(
            'devices' => $this->ether_wake->list_devices()
        );

        return $this->page->view_form('ether_wake/device_list', $data, lang('base_settings'));
    }

    function edit($schedule_id=null)
    {
        $data = array();
        $this->load->library('ether_wake/Form_Validation');
        $this->load->library('ether_wake/Ether_Wake');
        $this->load->library('ether_wake/Schedule');

        if(is_numeric($schedule_id)) {
            $data = $this->schedule->get($schedule_id, array('select_related' => true));
            $device_id = $data['device_id'];
            $data['schedule'] = $this->schedule->list_entries(array('device_id' => $device_id));
        
        } else if($this->input->post('device_id')) {
            $device_id = intval($this->input->post('device_id'), 10);
            $data = $this->ether_wake->get_device($device_id);
            $data['schedule'] = $this->schedule->list_entries(array('device_id' => $device_id));
        }

        $this->form_validation->set_policy('minute', 'ether_wake/Form_Validation', 'validate_schedule_minute', TRUE);
        $this->form_validation->set_policy('hour', 'ether_wake/Form_Validation', 'validate_schedule_hour', TRUE);
        $this->form_validation->set_policy('dom', 'ether_wake/Form_Validation', 'validate_schedule_dom', TRUE);
        $this->form_validation->set_policy('month', 'ether_wake/Form_Validation', 'validate_schedule_month', TRUE);
        $this->form_validation->set_policy('dow', 'ether_wake/Form_Validation', 'validate_schedule_dow', TRUE);

        $post = array(
            'device_id' => $this->input->post('device_id'),
            'active'    => $this->input->post('active'),
            'minute'    => $this->input->post('minute'),
            'hour'      => $this->input->post('hour'),
            'dom'       => $this->input->post('dom'),
            'month'     => $this->input->post('month'),
            'dow'       => $this->input->post('dow'),
        );

        if ($this->form_validation->run()) {
            $data = $this->schedule->save($post);
            $this->schedule->install_schedule();
            redirect('/ether_wake/device/edit/' . $data['device_id']);
        }

        $data = array_merge($data, $post);
        return $this->page->view_form('ether_wake/device_edit', $data, lang('base_settings'));
    }

    function add()
    {
        return $this->edit(null);
    }


    function delete($schedule_id)
    {
        $data = array();
        $this->load->library('ether_wake/Form_Validation');
        $this->load->library('ether_wake/Ether_Wake');
        $this->load->library('ether_wake/Schedule');

        if(is_numeric($schedule_id)) {
            $data = $this->schedule->get($schedule_id, array('select_related' => true));
        } 

        $this->form_validation->set_policy('schedule_id', 'ether_wake/Form_Validation', 'validate_schedule_id', TRUE);

        if ($this->form_validation->run() && $this->schedule->delete($schedule_id)) {
            $this->schedule->install_schedule();
            redirect('/ether_wake/device/edit/' . $data['device_id']);
        }

        return $this->page->view_form('ether_wake/schedule_delete', $data, lang('base_settings'));
    }
}
