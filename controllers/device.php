<?php

/**
 * ether_wake device controller.
 *
 * @category   apps
 * @package    app-ether_wake
 * @subpackage views
 * @author     AvanTech <fabio.montefuscolo@gmail.com>
 * @copyright  2018 AvanTech
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://gitlab.com/fabiomontefuscolo/app-ether-wake/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

use \Exception as Exception;

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * OpenSSH server settings controller.
 *
 * @category   apps
 * @package    ether-wake
 * @subpackage controllers
 * @author     Fabio Montefuscolo <fabio.montefuscolo@gmail.com>
 * @copyright  2018 Avantech
 */

class Device extends ClearOS_Controller
{
    function index()
    {
        $this->load->library('ether_wake/Ether_Wake');
        $this->load->library('ether_wake/Schedule');

        $this->schedule->install_schedule();

        $data = array(
            'devices' => $this->ether_wake->list_devices(),
            'schedule_content' => $this->schedule->get_schedule_content(),
            'schedule' => $this->schedule->list_entries(array('select_related' => true))
        );

        return $this->page->view_form('ether_wake/device_list', $data, lang('base_settings'));
    }

    function edit($device_id=null)
    {
        $data = array();
        $this->load->library('ether_wake/Form_Validation');
        $this->load->library('ether_wake/Ether_Wake');
        $this->load->library('ether_wake/Schedule');

        if(is_numeric($device_id)) {
            $data = $this->ether_wake->get_device($device_id);
            $data['schedule'] = $this->schedule->list_entries(array('device_id' => $device_id));
        }

        $this->form_validation->set_policy('device_id', 'ether_wake/Form_Validation', 'validate_device_id', FALSE);
        $this->form_validation->set_policy('name', 'ether_wake/Form_Validation', 'validate_device_name', TRUE);
        $this->form_validation->set_policy('address', 'ether_wake/Form_Validation', 'validate_device_address', TRUE);

        if ($this->form_validation->run()) {
            $data = $this->ether_wake->save_device(array(
                'device_id'   => $this->input->post('device_id'),
                'name'        => $this->input->post('name'),
                'address'     => $this->input->post('address'),
            ));
            redirect('/ether_wake/device/edit/' . $data['device_id']);
        }

        return $this->page->view_form('ether_wake/device_edit', $data, lang('base_settings'));
    }

    function add()
    {
        return $this->edit(null);
    }


    function delete($device_id)
    {
        $data = array();
        $this->load->library('ether_wake/Form_Validation');
        $this->load->library('ether_wake/Ether_Wake');

        if(is_numeric($device_id)) {
            $data = $this->ether_wake->get_device($device_id);
        } 

        $this->form_validation->set_policy('device_id', 'ether_wake/Form_Validation', 'validate_device_id', TRUE);

        if ($this->form_validation->run()
                && $this->ether_wake->delete_device($device_id)) {
            redirect('/ether_wake/device/');
        }


        return $this->page->view_form('ether_wake/device_delete', $data, lang('base_settings'));
    }

    function wake($device_id)
    {
        if(!is_numeric($device_id)) {
            redirect('/ether_wake/device/');
        }

        $this->load->library('ether_wake/Ether_Wake');

        if ($this->ether_wake->wake_device($device_id)){
            $this->page->set_message(lang('ether_wake_wol_packet_sent'), 'highlight');
        } else {
            $this->page->set_message(lang('ether_wake_wol_packet_failed'), 'critical');
        }

        redirect('/ether_wake/device/edit/' . $device_id);
    }
}
