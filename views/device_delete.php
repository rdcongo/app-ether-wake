<?php

/**
 * ether_wake device delete view.
 *
 * @category   apps
 * @package    app-ether_wake
 * @subpackage views
 * @author     AvanTech <fabio.montefuscolo@gmail.com>
 * @copyright  2018 AvanTech
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://gitlab.com/fabiomontefuscolo/app-ether-wake/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('ether_wake');

///////////////////////////////////////////////////////////////////////////////
// Form
///////////////////////////////////////////////////////////////////////////////

$read_only = false;
echo form_open('ether_wake/device/delete/' . $device_id);
echo form_header(lang('base_settings'));

echo infobox_critical(
     'Are you sure you want to delete this device?',
     '<table class="table theme-summary-table-large my-responsive dataTable collapsed dtr-inline">'
    .    '<tbody>'
    .           '<tr><th>Id         </th><td>' . $device_id   . '</td></tr>'
    .           '<tr><th>Name       </th><td>' . $name        . '</td></tr>'
    .           '<tr><th>Address    </th><td>' . $address     . '</td></tr>'
    .    '</tbody>'
    .'</table>'

    . field_input(
        'device_id',
        $device_id,
        lang('ether_wake_device_id'),
        $read_only,
        array('hide_field' => true )
    )

    . '<div class="text-right">'
    .    '<input type="submit" name="delete" value="Delete" class="btn btn-danger"/>'
    . '</div>'
);


echo form_footer();
echo form_close();
