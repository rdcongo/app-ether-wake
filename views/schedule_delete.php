<?php

/**
 * ether_wake schedule delete view.
 *
 * @category   apps
 * @package    app-ether_wake
 * @subpackage views
 * @author     AvanTech <fabio.montefuscolo@gmail.com>
 * @copyright  2018 AvanTech
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://gitlab.com/fabiomontefuscolo/app-ether-wake/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('ether_wake');

///////////////////////////////////////////////////////////////////////////////
// Form
///////////////////////////////////////////////////////////////////////////////

$read_only = false;
echo form_open('ether_wake/schedule/delete/' . $schedule_id);
echo form_header(lang('base_settings'));

echo infobox_critical(
     lang('ether_wake_delete_entry'),
     '<table class="table theme-summary-table-large my-responsive dataTable collapsed dtr-inline">'
    .    '<thead>'
    .           '<tr>'
    .               '<th>'. lang('ether_wake_device_name')     .'</th>'
    .               '<th>'. lang('ether_wake_schedule_minute') .'</th>'
    .               '<th>'. lang('ether_wake_schedule_hour')   .'</th>'
    .               '<th>'. lang('ether_wake_schedule_dom')    .'</th>'
    .               '<th>'. lang('ether_wake_schedule_month')  .'</th>'
    .               '<th>'. lang('ether_wake_schedule_dow')    .'</th>'
    .           '</tr>'
    .    '</thead>'
    .    '<tbody>'
    .           '<tr>'
    .               '<td>'. $name   .'</td>'
    .               '<td>'. $minute .'</td>'
    .               '<td>'. $hour   .'</td>'
    .               '<td>'. $dom    .'</td>'
    .               '<td>'. $month  .'</td>'
    .               '<td>'. $dow    .'</td>'
    .           '</tr>'
    .    '</tbody>'
    .'</table>'

    . field_input(
        'schedule_id',
        $schedule_id,
        lang('ether_wake_schedule_id'),
        $read_only,
        array('hide_field' => true )
    )

    . '<div class="text-right">'
    .    '<input type="submit" name="delete" value="Delete" class="btn btn-danger"/>'
    . '</div>'
);


echo form_footer();
echo form_close();
