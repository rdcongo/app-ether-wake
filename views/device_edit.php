<?php

/**
 * ether_wake device edit view.
 *
 * @category   apps
 * @package    app-ether_wake
 * @subpackage views
 * @author     AvanTech <fabio.montefuscolo@gmail.com>
 * @copyright  2018 AvanTech
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://gitlab.com/fabiomontefuscolo/app-ether-wake/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('ether_wake');

///////////////////////////////////////////////////////////////////////////////
// Form
///////////////////////////////////////////////////////////////////////////////

$read_only = false;
echo form_open('ether_wake/device/add');
echo form_header(lang('base_settings'));

echo field_input(
    'device_id',
    $device_id,
    lang('ether_wake_device_id'),
    $read_only,
    array('id' => 'ew_device_id', 'hide_field' => true )
);

echo field_input(
    'name',
    $name,
    lang('ether_wake_device_name'),
    $read_only,
    array('id' => 'ew_device_name')
);

echo field_input(
    'address',
    $address,
    lang('ether_wake_device_address'),
    $read_only,
    array('id' => 'ew_device_address')
);

echo field_button_set(array(
    $device_id > 0 ? form_submit_update('submit') : form_submit_add('submit'),
    $device_id > 0 ? anchor_custom('/app/ether_wake/device/wake/' . $device_id, 'Wake', 'link-only', array('class' => 'btn btn-success')) : '',
    anchor_cancel('/app/ether_wake'),
    anchor_delete('/app/ether_wake/device/delete/' . $device_id, 'link-only', array('class' => 'btn btn-danger')),
));

echo form_footer();
echo form_close();

// show schedule form just for already registered devices
if (empty($device_id)) {
    return;
}

if(!empty($schedule)) {
    echo summary_table(
        // table name
        lang('ether_wake_schedule'),

        NULL,

        // table headers
        array(
            lang('ether_wake_schedule_minute'), 
            lang('ether_wake_schedule_hour'), 
            lang('ether_wake_schedule_dom'), 
            lang('ether_wake_schedule_month'), 
            lang('ether_wake_schedule_dow'),
            ''
        ),

        array_map(function($entry){
            return array(
                'title' => $entry['name'],
                'action' => '',
                'anchors' => '',
                'details' => array(
                    $entry['minute'],
                    $entry['hour'],
                    $entry['dom'],
                    $entry['month'],
                    $entry['dow'],
                    anchor_custom(
                        '/app/ether_wake/schedule/delete/' . $entry['schedule_id'],
                        '✖️',
                        'link-only',
                        array('class' => 'btn btn-sm btn-danger')
                    ),
                )
            );
        }, $schedule),

        array(
            'no_action' => true,
            'read_only' => $read_only,
            'sort' => false
        )
    );
}


//
// Schedule form
//
echo form_open('ether_wake/schedule/add');
echo form_header(lang('ether_wake_schedule_add'));

echo field_input(
    'device_id',
    $device_id,
    lang('ether_wake_device_id'),
    $read_only,
    array('id' => 'ew_schedule_device_id', 'hide_field' => true )
);


echo field_input(
    'minute',
    $minute,
    lang('ether_wake_schedule_minute'),
    $read_only,
    array('id' => 'ew_schedule_minute')
);

echo field_input(
    'hour',
    $hour,
    lang('ether_wake_schedule_hour'),
    $read_only,
    array('id' => 'ew_schedule_hour')
);

echo field_input(
    'dom',
    $dom,
    lang('ether_wake_schedule_dom'),
    $read_only,
    array('id' => 'ew_schedule_dom')
);

echo field_input(
    'month',
    $month,
    lang('ether_wake_schedule_month'),
    $read_only,
    array('id' => 'ew_schedule_month')
);

echo field_input(
    'dow',
    $dow,
    lang('ether_wake_schedule_dow'),
    $read_only,
    array('id' => 'ew_schedule_dow')
);

echo form_submit_add('submit');

echo form_footer();
echo form_close();
