<?php

/**
 * ether_wake device list view.
 *
 * @category   apps
 * @package    app-ether_wake
 * @subpackage views
 * @author     AvanTech <fabio.montefuscolo@gmail.com>
 * @copyright  2018 AvanTech
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       https://gitlab.com/fabiomontefuscolo/app-ether-wake/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('ether_wake');

///////////////////////////////////////////////////////////////////////////////
// Form type handling
///////////////////////////////////////////////////////////////////////////////

$read_only = false;
echo str_replace('</table>', '</table><p class="text-right">' . anchor_add('/app/ether_wake/device/add') . '</p>',
    summary_table(
        // table name
        lang('ether_wake_device_list'),

        NULL,

        // table headers
        array(
            lang('ether_wake_device_name'), 
            lang('ether_wake_device_address'),
        ),

        array_map(function($device){
            return array(
                'title' => $device['name'],
                'action' => '',
                'anchors' => '',
                'details' => array(
                    anchor(
                        '/ether_wake/device/edit/' . $device['device_id'],
                        $device['name']
                    ),
                    $device['address'],
                )
            );
        }, $devices),

        array(
            'no_action' => true,
            'read_only' => $read_only
        )
    )
);


if(!empty($schedule)) {
    echo summary_table(
        // table name
        lang('ether_wake_schedule'),

        NULL,

        // table headers
        array(
            lang('ether_wake_device_name'), 
            lang('ether_wake_schedule_minute'), 
            lang('ether_wake_schedule_hour'), 
            lang('ether_wake_schedule_dom'), 
            lang('ether_wake_schedule_month'), 
            lang('ether_wake_schedule_dow'),
            ''
        ),

        array_map(function($entry){
            return array(
                'title' => $entry['name'],
                'action' => '',
                'anchors' => '',
                'details' => array(
                    anchor(
                        '/ether_wake/device/edit/' . $entry['device_id'],
                        $entry['name']
                    ),
                    $entry['minute'],
                    $entry['hour'],
                    $entry['dom'],
                    $entry['month'],
                    $entry['dow'],
                    anchor_custom(
                        '/app/ether_wake/schedule/delete/' . $entry['schedule_id'],
                        '✖️',
                        'link-only',
                        array('class' => 'btn btn-sm btn-danger')
                    ),
                )
            );
        }, $schedule),

        array(
            'no_action' => true,
            'read_only' => $read_only,
            'sort' => false
        )
    );
}


if(trim($schedule_content)) {
    echo box_open(lang('ether_wake_schedule_file_content'));
    echo box_content_open();
    echo box_content('<pre>' . $schedule_content . '</pre>');
    echo box_content_close();
    echo box_close();
}
